import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CheckboxFunctionality 
{

	int Var1;
	int Var2;
	ChromeDriver D;
	WebElement BMWCheckbox;
	@BeforeClass
	public void StartTest()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\VibrantMindsBatch3536\\Drivers\\chromedriver.exe");
		D=new ChromeDriver();
		D.get("https://courses.letskodeit.com/practice");
	}
	@Test(priority=1)
	public void TestIfCheckboxGetsChecked()
	{
		/*
		 * <input id="bmwcheck" value="bmw" name="cars" type="checkbox">
		 */
		BMWCheckbox=D.findElement(By.id("bmwcheck"));
		BMWCheckbox.click();
		/*
		 * isSelected() returns boolean true if a control is in SELECTED state else
		 * it returns boolean false
		 */
		boolean IsChecked=BMWCheckbox.isSelected();
		Assert.assertEquals(IsChecked, true,"Checkbox does not get selected after clicking on it..");
	}
	
	@Test(priority=2)
	public void TestIfCheckboxGetsUnchecked()
	{
		BMWCheckbox.click();
		boolean IsChecked=BMWCheckbox.isSelected();
		Assert.assertEquals(IsChecked, false,"Checkbox does not get deselected after clicking on it");
	}
	
	@AfterClass
	public void StopTest()
	{
		D.close();
	}
}
